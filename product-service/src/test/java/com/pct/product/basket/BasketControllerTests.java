package com.pct.product.basket;

import com.pct.product.domain.basket.Basket;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BasketControllerTests {

    @Autowired
    private ApplicationContext context;

    @InjectMocks
    private BasketController basketController;

    @Mock
    private BasketService basketService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(basketController).build();
    }

    @Test
    public void isPrototypeBean() {
        BasketController first = context.getBean(BasketController.class);
        BasketController second = context.getBean(BasketController.class);
        assertNotEquals(first, second);
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldAdd() throws Exception {
        Basket basket = new Basket();
        when(basketService.add("123", 3d)).thenReturn(basket);
        mockMvc.perform(post("/basket/add/123/quantity/3"))
                .andExpect(status().isOk());
    }
}
