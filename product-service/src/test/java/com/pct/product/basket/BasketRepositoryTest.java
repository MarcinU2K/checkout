package com.pct.product.basket;

import com.pct.product.domain.basket.Basket;
import com.pct.product.domain.basket.BasketStatus;
import com.pct.product.domain.product.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataMongoTest
public class BasketRepositoryTest {

	@Autowired
	private BasketRepository basketRepository;

	@Test
	public void shouldFindByAccountIdAndBasketStatus() {
		String accountId = "123";
		Product product = new Product("TestProduct", new BigDecimal(200));
		Basket basket = new Basket(BasketStatus.ACTIVE, product, 2d, accountId);
		Basket existingBasket = basketRepository.save(basket);
		Basket foundBasket = basketRepository.findByAccountIdAndBasketStatus(accountId, BasketStatus.ACTIVE);

		assertEquals(existingBasket.getProducts().size(), foundBasket.getProducts().size());
		assertEquals(existingBasket.getProducts().get(0).getName(), foundBasket.getProducts().get(0).getName());
		assertEquals(existingBasket.getAccountId(), foundBasket.getAccountId());
		assertEquals(existingBasket.getBasketStatus(), foundBasket.getBasketStatus());
	}

}