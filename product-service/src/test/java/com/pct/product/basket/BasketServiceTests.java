package com.pct.product.basket;

import com.pct.product.client.AccountServiceClient;
import com.pct.product.domain.basket.Basket;
import com.pct.product.domain.basket.BasketStatus;
import com.pct.product.domain.client.Account;
import com.pct.product.domain.product.Product;
import com.pct.product.exceptions.BasketException;
import com.pct.product.products.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BasketServiceTests {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ApplicationContext context;

    @InjectMocks
    private BasketService basketService;

    @Mock
    private AccountServiceClient accountServiceClient;

    @Mock
    private BasketRepository basketRepository;

    @Mock
    private ProductRepository productRepository;

    @Captor
    private ArgumentCaptor<Basket> basketArgumentCaptor;

    @Before
    public void setup() {
        initMocks(this);
    }

    @Test
    public void isPrototypeBean() {
        BasketController first = context.getBean(BasketController.class);
        BasketController second = context.getBean(BasketController.class);
        assertNotEquals(first, second);
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldAddNewBasket() throws BasketException {
        double quantity = 5d;
        Account account = getAccount();
        Product product = getSimpleProduct("1234","TestProduct");
        Optional<Product> optionalProduct = Optional.of(product);
        Basket basket = new Basket(BasketStatus.ACTIVE, product, quantity, account.getName());

        when(accountServiceClient.getAccountByName(anyString())).thenReturn(account);
        when(productRepository.findById(product.getId())).thenReturn(optionalProduct);
        when(basketRepository.findByAccountIdAndBasketStatus(account.getName(), BasketStatus.ACTIVE)).thenReturn(null);
        when(basketRepository.save(any(Basket.class))).thenReturn(basket);

        Basket result = basketService.add(product.getId(), quantity);

        verify(basketRepository, times(1)).save(any(Basket.class));
        assertEquals(account.getName(), result.getAccountId());
        assertEquals(optionalProduct.get(), result.getProducts().get(0));
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldAddNewProductToExistingBasket() throws BasketException {
        double quantity = 5d;
        Account account = getAccount();
        Product product = getSimpleProduct("1234", "TestProduct");
        Product differentProduct = getSimpleProduct("123", "TestProduct2");
        Optional<Product> optionalProduct = Optional.of(differentProduct);
        Basket basket = new Basket(BasketStatus.ACTIVE, product, quantity, account.getName());
        Basket updatedBasket = new Basket(BasketStatus.ACTIVE, product, quantity, account.getName());
        updatedBasket.addProducts(differentProduct, quantity);

        when(accountServiceClient.getAccountByName(anyString())).thenReturn(account);
        when(productRepository.findById(differentProduct.getId())).thenReturn(optionalProduct);
        when(basketRepository.findByAccountIdAndBasketStatus(account.getName(), BasketStatus.ACTIVE)).thenReturn(basket);
        when(basketRepository.save(any(Basket.class))).thenReturn(updatedBasket);

        Basket result = basketService.add(differentProduct.getId(), quantity);

        verify(basketRepository, times(1)).save(any(Basket.class));
        assertEquals(account.getName(), result.getAccountId());
        assertEquals(2, result.getProducts().size());
        assertEquals(product.getId(), result.getProducts().get(0).getId());
        assertEquals(differentProduct.getId(), result.getProducts().get(1).getId());
        assertEquals(product.getQuantity(), result.getProducts().get(0).getQuantity());
        assertEquals(differentProduct.getQuantity(), result.getProducts().get(1).getQuantity());
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldAddQuantityToExistingProductInExistingBasket() throws BasketException {
        double quantity = 5d;
        Account account = getAccount();
        Product product = getSimpleProduct("12345", "TestProduct");
        Product sameProduct = getSimpleProduct("12345", "TestProduct");
        Optional<Product> optionalProduct = Optional.of(product);
        Basket existingBasket = new Basket(BasketStatus.ACTIVE, product, quantity, account.getName());
        Basket updatedBasket = new Basket(BasketStatus.ACTIVE, sameProduct, quantity + quantity, account.getName());

        when(accountServiceClient.getAccountByName(anyString())).thenReturn(account);
        when(productRepository.findById(product.getId())).thenReturn(optionalProduct);
        when(basketRepository.findByAccountIdAndBasketStatus(account.getName(), BasketStatus.ACTIVE)).thenReturn(existingBasket);
        when(basketRepository.save(any(Basket.class))).thenReturn(updatedBasket);

        Basket result = basketService.add(product.getId(), quantity);

        verify(basketRepository, times(1)).save(basketArgumentCaptor.capture());
        assertEquals(account.getName(), result.getAccountId());
        assertEquals(2 * quantity, result.getProducts().get(0).getQuantity());
        log.info(basketArgumentCaptor.getValue().toString());
    }

    @Test(expected = BasketException.class)
    @WithMockUser("johnkowalski")
    public void shouldThrowErrorIfAccountNotFound() throws BasketException {
        when(accountServiceClient.getAccountByName(anyString())).thenReturn(null);
        basketService.add("11", 2d);
    }

    private Account getAccount() {
        Account account = new Account();
        account.setName("johnkowalski");
        account.setLastSeen(new Date());
        return account;
    }

    private Product getSimpleProduct(String id, String name) {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setPrice(BigDecimal.valueOf(100));
        return product;
    }
}
