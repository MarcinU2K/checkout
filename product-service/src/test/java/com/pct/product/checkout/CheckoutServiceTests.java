package com.pct.product.checkout;

import com.pct.product.basket.BasketRepository;
import com.pct.product.bundle.BundleRepository;
import com.pct.product.client.AccountServiceClient;
import com.pct.product.domain.basket.Basket;
import com.pct.product.domain.basket.BasketStatus;
import com.pct.product.domain.bundle.Bundle;
import com.pct.product.domain.checkout.Checkout;
import com.pct.product.domain.client.Account;
import com.pct.product.domain.product.Product;
import com.pct.product.exceptions.CheckoutException;
import com.pct.product.products.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.LoggingEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CheckoutServiceTests {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ApplicationContext context;

    @InjectMocks
    private CheckoutService checkoutService;

    @Mock
    private AccountServiceClient accountServiceClient;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private BasketRepository basketRepository;

    @Mock
    private BundleRepository bundleRepository;

    @Captor
    private ArgumentCaptor<LoggingEvent> logArgumentCaptor;

    @Before
    public void setup() {
        initMocks(this);
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldCalculateSimpleCharge() throws CheckoutException {
        double quantity = 5d;
        Account account = getAccount();
        Product product = getSimpleProduct("1234","TestProduct");
        Basket basket = new Basket(BasketStatus.ACTIVE, product, quantity, account.getName());

        when(basketRepository.findByAccountIdAndBasketStatus(account.getName(), BasketStatus.ACTIVE)).thenReturn(basket);

        Checkout result = checkoutService.calculateCharge(account.getName());

        log.info(result.toString());
        assertEquals(product.getPrice().multiply(new BigDecimal(quantity, MathContext.DECIMAL64)), result.getTotalAmount());
    }

    @Test(expected = CheckoutException.class)
    @WithMockUser("johnkowalski")
    public void shouldNotCalculateSimpleCharge() throws CheckoutException {
        when(basketRepository.findByAccountIdAndBasketStatus("johnkowalski", BasketStatus.ACTIVE)).thenReturn(null);
        checkoutService.calculateCharge("1234");
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldCalculateMultibuyCharge() throws CheckoutException {
        double quantity = 11d;
        Account account = getAccount();
        Product product = getMultibuyProduct("1234","TestProduct");
        Basket basket = new Basket(BasketStatus.ACTIVE, product, quantity, account.getName());

        when(basketRepository.findByAccountIdAndBasketStatus(account.getName(), BasketStatus.ACTIVE)).thenReturn(basket);

        Checkout result = checkoutService.calculateCharge(account.getName());

        log.info(result.toString());
        assertEquals(product.getMultibuyPrice(), result.getTotalAmount().subtract(product.getPrice()));
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldCalculateSaving() throws CheckoutException {
        double quantity = 1d;
        Account account = getAccount();
        Product firstProduct = getBundledProduct("123", "321", "TestProduct1");
        Product secondProduct = getBundledProduct("321", "123", "TestProduct1");
        Basket basket = new Basket(BasketStatus.ACTIVE, firstProduct, quantity, account.getName());
        basket.addProducts(secondProduct, 1d);

        Bundle bundle = new Bundle(BigDecimal.valueOf(30), Arrays.asList(firstProduct.getId(), secondProduct.getId()));
        bundle.setId("567");
        Optional<Bundle> bundleOpt = Optional.of(bundle);

        when(basketRepository.findByAccountIdAndBasketStatus(account.getName(), BasketStatus.ACTIVE)).thenReturn(basket);
        when(bundleRepository.findById(anyString())).thenReturn(bundleOpt);

        Checkout result = checkoutService.calculateCharge(account.getName());

        log.info(result.toString());
        assertEquals(firstProduct.getPrice().add(secondProduct.getPrice()), result.getTotalAmount());
        assertEquals(firstProduct.getPrice().add(secondProduct.getPrice()).subtract(bundle.getSaving()), result.getTotalAmountAfterSavings());
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldNotCalculateSavingIfBundleDoesNotExist() throws CheckoutException {
        double quantity = 1d;
        Account account = getAccount();
        Product firstProduct = getBundledProduct("123", "321", "TestProduct1");
        Product secondProduct = getBundledProduct("321", "123", "TestProduct1");
        Basket basket = new Basket(BasketStatus.ACTIVE, firstProduct, quantity, account.getName());
        basket.addProducts(secondProduct, 1d);

        Optional<Bundle> bundleOpt = Optional.empty();

        when(basketRepository.findByAccountIdAndBasketStatus(account.getName(), BasketStatus.ACTIVE)).thenReturn(basket);
        when(bundleRepository.findById(anyString())).thenReturn(bundleOpt);

        Checkout result = checkoutService.calculateCharge(account.getName());

        log.info(result.toString());
        assertEquals(firstProduct.getPrice().add(secondProduct.getPrice()), result.getTotalAmountAfterSavings());
    }

    private Account getAccount() {
        Account account = new Account();
        account.setName("johnkowalski");
        account.setLastSeen(new Date());
        return account;
    }

    private Product getSimpleProduct(String id, String name) {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setPrice(BigDecimal.valueOf(100));
        return product;
    }

    private Product getMultibuyProduct(String id, String name) {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setPrice(BigDecimal.valueOf(100));
        product.setMultibuyPrice(BigDecimal.valueOf(500));
        product.setMultibuyQuantity(10d);
        return product;
    }

    private Product getBundledProduct(String firstProduct, String secondProduct, String name) {
        Product product = new Product();
        product.setId(firstProduct);
        product.setName(name);
        product.setPrice(BigDecimal.valueOf(100));
        product.addDiscountBundle(secondProduct, "567");
        return product;
    }
}
