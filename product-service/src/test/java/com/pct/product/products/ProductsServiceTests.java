package com.pct.product.products;

import com.pct.product.ProductServiceApplication;
import com.pct.product.domain.product.Product;
import com.pct.product.products.ProductRepository;
import com.pct.product.products.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductServiceApplication.class)
public class ProductsServiceTests {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Before
    public void setup() {
        initMocks(this);
    }


    @Test
    @WithMockUser("johnkowalski")
    public void shouldAddProduct() {
        Product product = getSimpleProduct();
        productService.add(product);
        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    public void shouldGetAll() {
        Product product = getSimpleProduct();
        Product product2 = getSimpleProduct();
        List<Product> products = new ArrayList<>();
        products.add(product);
        products.add(product2);

        when(productRepository.findAll()).thenReturn(products);

        List<Product> result = productService.getAll();
        assertEquals(products.get(0).getId(), result.get(0).getId());
        assertEquals(products.get(1).getId(), result.get(1).getId());
    }

    private Product getSimpleProduct() {
        Product product = new Product();
        product.setName("TestProduct");
        product.setPrice(BigDecimal.valueOf(100));
        return product;
    }
}
