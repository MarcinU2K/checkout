package com.pct.product.products;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pct.product.domain.product.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductsControllerTests {

    @Autowired
    private ObjectMapper mapper;

    @InjectMocks
    private ProductController productController;

    @Mock
    private ProductService productService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    public void shouldAdd() throws Exception {
        Product product = new Product(
                "testName",
                new BigDecimal(200),
                new BigDecimal(300),
                4d);

        when(productService.add(product)).thenReturn(product);

        String json = mapper.writeValueAsString(product);

        mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldGetAll() throws Exception {
        Product product = new Product(
                "testName",
                new BigDecimal(200),
                new BigDecimal(300),
                4d);
        Product product2 = new Product(
                "testName",
                new BigDecimal(200),
                new BigDecimal(300),
                4d);

        List<Product> products = new ArrayList<>();
        products.add(product);
        products.add(product2);

        when(productService.getAll()).thenReturn(products);

        mockMvc.perform(get("/all"))
                .andExpect(status().isOk());
    }
}
