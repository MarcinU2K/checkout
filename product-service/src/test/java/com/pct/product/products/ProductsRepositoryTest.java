package com.pct.product.products;

import com.pct.product.domain.product.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataMongoTest
public class ProductsRepositoryTest {

	@Autowired
	private ProductRepository productRepository;

	@Test
	public void shouldFindAll() {
		Product product = getSimpleProduct();
		Product product2 = getSimpleProduct();
		List<Product> products = new ArrayList<>();
		products.add(product);
		products.add(product2);

		productRepository.saveAll(products);

		List<Product> foundProducts = productRepository.findAll();
		assertEquals(products.size(), foundProducts.size());
		assertEquals(products.get(0).getId(), foundProducts.get(0).getId());
	}

	private Product getSimpleProduct() {
		Product product = new Product();
		product.setName("TestProduct");
		product.setPrice(BigDecimal.valueOf(100));
		return product;
	}
}