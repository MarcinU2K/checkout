package com.pct.product.bundle;

import com.pct.product.domain.bundle.Bundle;
import com.pct.product.domain.product.Product;
import com.pct.product.exceptions.BundleException;
import com.pct.product.products.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BundleServiceTests {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @InjectMocks
    private BundleService bundleService;

    @Mock
    private BundleRepository bundleRepository;

    @Mock
    private ProductRepository productRepository;

    @Captor
    private ArgumentCaptor<Product> productArgumentCaptor;

    @Before
    public void setup() {
        initMocks(this);
    }

    @Test
    @WithMockUser("johnkowalski")
    public void shouldAdd() throws BundleException {
        Product product1 = getSimpleProduct("123");
        Product product2 = getSimpleProduct("321");
        Product product1updated = getSimpleProduct("123");
        Product product2updated = getSimpleProduct("321");
        Optional<Product> optProduct1 = Optional.of(product1);
        Optional<Product> optProduct2 = Optional.of(product2);

        List<String> bundledProductIds = new ArrayList<>();
        bundledProductIds.add("123");
        bundledProductIds.add("321");
        Bundle newBundle = new Bundle(BigDecimal.valueOf(200), bundledProductIds);
        newBundle.setId("567");
        product1updated.addDiscountBundle(product2.getId(), newBundle.getId());
        product2updated.addDiscountBundle(product1.getId(), newBundle.getId());

        when(bundleRepository.save(any(Bundle.class))).thenReturn(newBundle);
        when(productRepository.findById(anyString())).thenReturn(optProduct1);
        when(productRepository.save(any(Product.class))).thenReturn(product1updated);
        when(productRepository.findById(anyString())).thenReturn(optProduct2);
        when(productRepository.save(any(Product.class))).thenReturn(product2updated);


        Bundle result = bundleService.add(newBundle);

        verify(bundleRepository, times(1)).save(any(Bundle.class));
        verify(productRepository, times(2)).save(productArgumentCaptor.capture());

        List<Product> capturedProducts = productArgumentCaptor.getAllValues();
        capturedProducts.forEach(product -> log.info(product.toString()));
        result.getProducts().forEach(log::info);
    }

    private Product getSimpleProduct(String id) {
        Product product = new Product();
        product.setId(id);
        product.setName("TestProduct");
        product.setPrice(BigDecimal.valueOf(100));
        return product;
    }
}
