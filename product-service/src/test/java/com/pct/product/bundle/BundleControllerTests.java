package com.pct.product.bundle;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pct.product.domain.bundle.Bundle;
import com.pct.product.domain.product.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BundleControllerTests {

    @Autowired
    private ObjectMapper mapper;

    @InjectMocks
    private BundleController bundleController;

    @Mock
    private BundleService bundleService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(bundleController).build();
    }

    @Test
    public void shouldAdd() throws Exception {
        List<String> productIds = new ArrayList<>(2);
        productIds.add("123");
        productIds.add("321");
        Bundle bundle = new Bundle(new BigDecimal(123), productIds);
        when(bundleService.add(bundle)).thenReturn(bundle);
        String json = mapper.writeValueAsString(bundle);
        mockMvc.perform(post("/bundle/add").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk());
    }
}
