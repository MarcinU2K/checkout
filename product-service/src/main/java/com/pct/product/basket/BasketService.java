package com.pct.product.basket;

import com.pct.product.client.AccountServiceClient;
import com.pct.product.domain.basket.Basket;
import com.pct.product.domain.basket.BasketAction;
import com.pct.product.domain.basket.BasketErrorResponse;
import com.pct.product.domain.basket.BasketStatus;
import com.pct.product.domain.client.Account;
import com.pct.product.domain.product.Product;
import com.pct.product.exceptions.BasketException;
import com.pct.product.products.ProductRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Scope(value="prototype")
public class BasketService {

    Logger log = LoggerFactory.getLogger(getClass());

    private static final String PRODUCT_NOT_FOUND = "PRODUCT_NOT_FOUND";
    private static final String THE_PRODUCT_WITH_ID = "The product with id ";
    private static final String HAS_NOT_BEEN_FOUND = " has not been found";
    private static final String ACCOUNT_NOT_FOUND = "ACCOUNT_NOT_FOUND";
    private static final String THE_ACCOUNT = "The account ";
    private static final String UNKNOWN_CONTEXT = "unknown context";
    public static final String INVALID_BASKET_ACTION = "INVALID_BASKET_ACTION";
    public static final String THE_ACTION = "The action ";
    public static final String HAS_NOT_BEEN_IMPLEMENTED = " has not been implemented";

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private AccountServiceClient accountServiceClient;

    public Basket add(String productId, Double quantity) throws BasketException {
        Account currentAccount = getAccount();
        Product existingProduct = getProduct(productId);
        return createOrUpdateBasket(quantity, currentAccount, existingProduct, BasketAction.ADD);
    }

    private Basket createOrUpdateBasket(Double quantity, Account currentAccount, Product existingProduct, BasketAction basketAction) throws BasketException {
        if (BasketAction.ADD.equals(basketAction)) {
            Basket existingBasket = basketRepository.findByAccountIdAndBasketStatus(currentAccount.getName(), BasketStatus.ACTIVE);
            if (existingBasket == null) {
                Basket newBasket = new Basket(BasketStatus.ACTIVE, existingProduct, quantity, currentAccount.getName());
                return basketRepository.save(newBasket);
            }
            for (Product product : existingBasket.getProducts()) {
                if (existingProduct.getId().equals(product.getId())) {
                    product.setQuantity(product.getQuantity() + quantity);
                    return basketRepository.save(existingBasket);
                }
            }
            existingBasket.addProducts(existingProduct, quantity);
            return basketRepository.save(existingBasket);
        }
        throw new BasketException(new BasketErrorResponse(
                500,
                INVALID_BASKET_ACTION,
                THE_ACTION + basketAction + HAS_NOT_BEEN_IMPLEMENTED));
    }

    private Product getProduct(String productId) throws BasketException {
        return productRepository.findById(productId).orElseThrow(() -> new BasketException(new BasketErrorResponse(
                404,
                PRODUCT_NOT_FOUND,
                THE_PRODUCT_WITH_ID + productId + HAS_NOT_BEEN_FOUND)));
    }

    private Account getAccount() throws BasketException {
        Account currentAccount = accountServiceClient.getAccountByName(getCurrentUserNameFromSecurityContext());
        if (currentAccount == null) {
            throw new BasketException(new BasketErrorResponse(
                    500,
                    ACCOUNT_NOT_FOUND,
                    THE_ACCOUNT + getCurrentUserNameFromSecurityContext() + HAS_NOT_BEEN_FOUND));
        }
        return currentAccount;
    }

    private String getCurrentUserNameFromSecurityContext() {
        return StringUtils.isNotEmpty(SecurityContextHolder.getContext().getAuthentication().getName()) ?
                SecurityContextHolder.getContext().getAuthentication().getName() :
                UNKNOWN_CONTEXT;
    }
}