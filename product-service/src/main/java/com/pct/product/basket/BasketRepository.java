package com.pct.product.basket;

import com.pct.product.domain.basket.Basket;
import com.pct.product.domain.basket.BasketStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BasketRepository extends MongoRepository<Basket, String> {
    Basket findByAccountIdAndBasketStatus(String accountId, BasketStatus basketStatus);
}
