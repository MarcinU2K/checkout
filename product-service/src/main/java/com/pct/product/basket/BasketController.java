package com.pct.product.basket;

import com.pct.product.domain.basket.Basket;
import com.pct.product.domain.basket.BasketErrorResponse;
import com.pct.product.domain.product.Product;
import com.pct.product.exceptions.BasketException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/basket")
@Scope("prototype")
public class BasketController {

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private BasketService basketService;

    @PostMapping("/add/{productId}/quantity/{quantity}")
    public ResponseEntity<Basket> add(@PathVariable String productId, @PathVariable Double quantity) {
        //TODO cyclic job removing basket after X amount of time
        try {
            return ResponseEntity.ok(basketService.add(productId, quantity));
        } catch (BasketException basketException) {
            return ResponseEntity.status(basketException.getErrorResponse().getCode()).body(basketException.getErrorResponse());
        } catch (Throwable unhandledException) {
            log.error(unhandledException.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new BasketErrorResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    unhandledException.getClass().getSimpleName().toUpperCase(),
                    unhandledException.getMessage()));
        }
    }

    @PutMapping("/remove/{productId}")
    public ResponseEntity<Basket> remove(@PathVariable String productId) {
        //TODO removing basket
        return notImplemented();
    }

    @PutMapping("/edit/{productId}")
    public ResponseEntity<Basket> edit(@RequestBody Product product) {
        //TODO editing basket
        return notImplemented();
    }

    private ResponseEntity<Basket> notImplemented() {
        try {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(null);
        } catch (Throwable unhandledException) {
            log.error(unhandledException.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
