package com.pct.product.checkout;

import com.pct.product.basket.BasketRepository;
import com.pct.product.domain.bundle.Bundle;
import com.pct.product.domain.basket.Basket;
import com.pct.product.domain.basket.BasketStatus;
import com.pct.product.domain.checkout.Checkout;
import com.pct.product.domain.checkout.CheckoutErrorResponse;
import com.pct.product.domain.checkout.Item;
import com.pct.product.domain.product.Product;
import com.pct.product.exceptions.CheckoutException;
import com.pct.product.bundle.BundleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CheckoutService {
    public static final String HAS_NOT_BEEN_FOUND = " has not been found";
    public static final String THE_BASKET_FOR_ACCOUNT = "The basket for account ";
    public static final String BASKET_NOT_FOUND = "BASKET_NOT_FOUND";

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private BundleRepository bundleRepository;


    public Checkout calculateCharge(String accountId) throws CheckoutException {
        List<Item> returnItems;
        Basket existingBasket = getBasket(accountId);
        List<Product> basketProducts = existingBasket.getProducts();
        List<Product> multibuyProducts = basketProducts.stream().filter(this::isMultibuyEligible).collect(Collectors.toList());
        List<Product> multibuyRemainingProducts = new ArrayList<>();
        BigDecimal totalAmount = BigDecimal.valueOf(0);
        BigDecimal totalSaving = BigDecimal.valueOf(0);
        totalSaving = totalSaving.add(calculateTotalSavings(basketProducts, totalSaving));
        totalAmount = totalAmount.add(calculateMultibuyProducts(multibuyRemainingProducts, multibuyProducts, totalAmount));
        basketProducts.removeAll(multibuyProducts);
        totalAmount = calculateBasketProducts(basketProducts, totalAmount);
        returnItems = prepareProductList(basketProducts, multibuyProducts, multibuyRemainingProducts);
        return new Checkout(returnItems, totalAmount, totalSaving);
    }

    private List<Item> prepareProductList(List<Product> productsFromBasket, List<Product> multibuyProducts, List<Product> multibuyRemainingProducts) {
        List<Item> products = new ArrayList<>();
        productsFromBasket.forEach(product -> products.add(new Item(
                product.getName(),
                product.getQuantity(),
                product.getPrice(),
                false)));
        multibuyProducts.forEach(multibuyProduct -> products.add(new Item(
                multibuyProduct.getName(),
                multibuyProduct.getQuantity(),
                multibuyProduct.getPrice(),
                true,
                multibuyProduct.getMultibuyPrice(),
                multibuyProduct.getMultibuyQuantity(),
                (int) (multibuyProduct.getQuantity() / multibuyProduct.getMultibuyQuantity()))));
        if (!multibuyRemainingProducts.isEmpty()) {
            multibuyRemainingProducts.forEach(multibuyRemainingProduct -> products.add(new Item(
                    multibuyRemainingProduct.getName(),
                    multibuyRemainingProduct.getQuantity(),
                    multibuyRemainingProduct.getPrice(),
                    false)));

        }
        return products;
    }

    private BigDecimal calculateBasketProducts(List<Product> productsFromBasket, BigDecimal totalAmount) {
        for (Product basketProduct : productsFromBasket) {
            totalAmount = totalAmount.add(basketProduct.getPrice().multiply(new BigDecimal(basketProduct.getQuantity(), MathContext.DECIMAL64)));
        }
        return totalAmount;
    }

    private BigDecimal calculateTotalSavings(List<Product> productsFromBasket, BigDecimal totalSaving) {
        List<Product> bundledProductsFromBasket = productsFromBasket.stream().filter(product -> product.getDiscountBundles() != null).collect(Collectors.toList());
        if (bundledProductsFromBasket.size() > 0) {
            List<String> productIdsForSavingsUsed = new ArrayList<>();
            for (Product product : bundledProductsFromBasket) {
                for (Map.Entry<String, String> bundle : product.getDiscountBundles().entrySet()) {
                    if (!productIdsForSavingsUsed.contains(product.getId())) {
                        Bundle existingBundle = checkIfBasketContainsAnyMatchingProducts(bundle, bundledProductsFromBasket);
                        if (existingBundle != null) {
                            totalSaving = totalSaving.add(existingBundle.getSaving());
                            productIdsForSavingsUsed.add(bundle.getKey());
                        } else {
                            log.error("Bundle {} for product {} does not exist.", bundle.getValue(), product.getId());
                        }
                    }
                }
            }
        }
        return totalSaving;
    }

    private Bundle checkIfBasketContainsAnyMatchingProducts(Map.Entry<String, String> bundle, List<Product> bundledProductsFromBasket) {
        Bundle existingBundle = null;
        for (Product product : bundledProductsFromBasket) {
            if (bundle.getKey().equals(product.getId())) {
                existingBundle = bundleRepository.findById(bundle.getValue()).orElse(null);
                break;
            }
        }
        log.info("existingBundle: " + existingBundle);
        return existingBundle;
    }

    private BigDecimal calculateMultibuyProducts(List<Product> multibuyRemainingProducts, List<Product> multibuyProducts, BigDecimal totalAmount) {
        if (multibuyProducts.size() > 0) {
            for (Product multibuyProduct : multibuyProducts) {
                double genuineQuantity = multibuyProduct.getQuantity();
                double remainingQuantity = genuineQuantity;
                while (true) {
                    if (remainingQuantity >= multibuyProduct.getMultibuyQuantity()) {
                        remainingQuantity = remainingQuantity - multibuyProduct.getMultibuyQuantity();
                        multibuyProduct.setQuantity(remainingQuantity);
                        totalAmount = totalAmount.add(multibuyProduct.getMultibuyPrice());
                    } else {
                        BigDecimal remainingCharge = multibuyProduct.getPrice().multiply(new BigDecimal(remainingQuantity, MathContext.DECIMAL64));
                        totalAmount = totalAmount.add(remainingCharge);
                        multibuyProduct.setQuantity(genuineQuantity - remainingQuantity);
                        if (remainingQuantity > 0) {
                            multibuyRemainingProducts.add(new Product(
                                    multibuyProduct.getName(),
                                    multibuyProduct.getPrice(),
                                    multibuyProduct.getMultibuyPrice(),
                                    multibuyProduct.getMultibuyQuantity(),
                                    remainingQuantity));
                        }
                        break;
                    }
                }
            }
        }
        return totalAmount;
    }

    private boolean isMultibuyEligible(Product basketProduct) {
        return basketProduct.getMultibuyQuantity() != null &&
                basketProduct.getMultibuyPrice() != null &&
                basketProduct.getMultibuyPrice().compareTo(BigDecimal.ZERO) != 0 &&
                basketProduct.getQuantity() != null &&
                basketProduct.getQuantity() >= basketProduct.getMultibuyQuantity();
    }

    private Basket getBasket(String accountId) throws CheckoutException {
        Basket existingBasket = basketRepository.findByAccountIdAndBasketStatus(accountId, BasketStatus.ACTIVE);
        if (existingBasket == null) {
            throw new CheckoutException(new CheckoutErrorResponse(
                    404,
                    BASKET_NOT_FOUND,
                    THE_BASKET_FOR_ACCOUNT + accountId + HAS_NOT_BEEN_FOUND));
        }
        return existingBasket;
    }
}
