package com.pct.product.checkout;

import com.pct.product.domain.checkout.Checkout;
import com.pct.product.domain.checkout.CheckoutErrorResponse;
import com.pct.product.exceptions.CheckoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/checkout")
@RestController
public class CheckoutController {

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private CheckoutService checkoutService;

    @PutMapping("/{accountId}")
    public ResponseEntity<Checkout> calculateCharge(@PathVariable String accountId) {
        try {
            return ResponseEntity.ok(checkoutService.calculateCharge(accountId));
        } catch (CheckoutException checkoutException) {
            return ResponseEntity.status(checkoutException.getErrorResponse().getCode()).body(checkoutException.getErrorResponse());
        } catch (Throwable unhandledException) {
            log.error(unhandledException.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CheckoutErrorResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    unhandledException.getClass().getSimpleName().toUpperCase(),
                    unhandledException.getMessage()));
        }
    }
    //TODO payment endpoint
}