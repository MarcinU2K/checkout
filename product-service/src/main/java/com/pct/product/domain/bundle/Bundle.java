package com.pct.product.domain.bundle;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "bundles")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Bundle {

    @Id
    private String id;

    @Version
    private Integer version;

    @NotNull
    private BigDecimal saving;

    @NotNull
    private List<String> products;

    public Bundle() {
    }

    public Bundle(@NotNull BigDecimal saving, @NotNull List<String> products) {
        this.saving = saving;
        setProducts(products);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public BigDecimal getSaving() {
        return saving;
    }

    public void setSaving(BigDecimal saving) {
        this.saving = saving;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        if(this.products == null) {
            this.products = new ArrayList<>();
        }
        this.products.addAll(products);
    }

    @Override
    public String toString() {
        return "Bundle{" +
                "id='" + id + '\'' +
                ", version=" + version +
                ", saving=" + saving +
                ", products=" + products +
                '}';
    }
}