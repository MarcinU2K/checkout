package com.pct.product.domain.checkout;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Checkout {

    private List<Item> items;

    private BigDecimal totalAmount;

    private BigDecimal totalSaving;

    private BigDecimal totalAmountAfterSavings;

    public Checkout() {
    }

    public Checkout(List<Item> items, BigDecimal totalAmount, BigDecimal totalSaving) {
        this.items = items;
        this.totalAmount = totalAmount;
        this.totalSaving = totalSaving;
        this.totalAmountAfterSavings = totalAmount.subtract(totalSaving);
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalSaving() {
        return totalSaving;
    }

    public void setTotalSaving(BigDecimal totalSaving) {
        this.totalSaving = totalSaving;
    }

    public BigDecimal getTotalAmountAfterSavings() {
        return totalAmountAfterSavings;
    }

    public void setTotalAmountAfterSavings(BigDecimal totalAmountAfterSavings) {
        this.totalAmountAfterSavings = totalAmountAfterSavings;
    }

    @Override
    public String toString() {
        return "Checkout{" +
                "items=" + items +
                ", totalAmount=" + totalAmount +
                ", totalSaving=" + totalSaving +
                ", totalAmountAfterSavings=" + totalAmountAfterSavings +
                '}';
    }
}
