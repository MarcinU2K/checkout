package com.pct.product.domain.product;

public class ProductErrorResponse extends Product {

    private int code;

    private String error;

    private String desc;

    public ProductErrorResponse() {
    }

    public ProductErrorResponse(int code, String error, String desc) {
        this.code = code;
        this.error = error;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}