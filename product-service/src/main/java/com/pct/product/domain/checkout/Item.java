package com.pct.product.domain.checkout;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Item {
    private String name;
    private Double quantity;
    private BigDecimal price;
    private boolean multibuy;
    private BigDecimal multibuyPrice;
    private Double multibuyQuantity;
    private Integer multibuyRatio;
    private BigDecimal multibuyTotal;


    public Item() {
    }

    public Item(String name, Double quantity, BigDecimal price, boolean multibuy) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.multibuy = multibuy;
    }

    public Item(String name, Double quantity, BigDecimal price, boolean multibuy, BigDecimal multibuyPrice, Double multibuyQuantity, Integer multibuyRatio) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.multibuy = multibuy;
        this.multibuyPrice = multibuyPrice;
        this.multibuyQuantity = multibuyQuantity;
        this.multibuyRatio = multibuyRatio;
        this.multibuyTotal =  multibuyPrice.multiply(new BigDecimal(multibuyRatio));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isMultibuy() {
        return multibuy;
    }

    public void setMultibuy(boolean multibuy) {
        this.multibuy = multibuy;
    }

    public BigDecimal getMultibuyPrice() {
        return multibuyPrice;
    }

    public void setMultibuyPrice(BigDecimal singleMultibuyPrice) {
        this.multibuyPrice = singleMultibuyPrice;
    }

    public Double getMultibuyQuantity() {
        return multibuyQuantity;
    }

    public void setMultibuyQuantity(Double multibuyQuantity) {
        this.multibuyQuantity = multibuyQuantity;
    }

    public Integer getMultibuyRatio() {
        return multibuyRatio;
    }

    public void setMultibuyRatio(Integer multibuyRatio) {
        this.multibuyRatio = multibuyRatio;
    }

    public BigDecimal getMultibuyTotal() {
        return multibuyTotal;
    }

    public void setMultibuyTotal(BigDecimal multibuyTotal) {
        this.multibuyTotal = multibuyTotal;
    }
}
