package com.pct.product.domain.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Document(collection = "products")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product {

    @Id
    private String id;

    @Version
    private Integer version;

    private Date createdDate;

    private Date lastModifiedDate;

    @NotNull
    private String name;

    private Double quantity;

    @NotNull
    private BigDecimal price;

    private BigDecimal multibuyPrice = BigDecimal.ZERO;

    private Double multibuyQuantity;

    private Map<String, String> discountBundles;

    public Product() {
    }

    public Product(@NotNull String name, @NotNull BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public Product(@NotNull String name, @NotNull BigDecimal price, BigDecimal multibuyPrice, Double multibuyQuantity) {
        this.createdDate = new Date();
        this.name = name;
        this.price = price;
        this.multibuyPrice = multibuyPrice;
        this.multibuyQuantity = multibuyQuantity;
    }

    public Product(@NotNull String name, @NotNull BigDecimal price, BigDecimal multibuyPrice, Double multibuyQuantity, Double quantity) {
        this.createdDate = new Date();
        this.name = name;
        this.price = price;
        this.multibuyPrice = multibuyPrice;
        this.multibuyQuantity = multibuyQuantity;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public void addQuantity(Double quantity) {
        this.quantity = this.quantity + quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMultibuyPrice() {
        return multibuyPrice;
    }

    public void setMultibuyPrice(BigDecimal multibuyPrice) {
        this.multibuyPrice = multibuyPrice;
    }

    public Double getMultibuyQuantity() {
        return this.multibuyQuantity != null && this.multibuyQuantity != 0 ? this.multibuyQuantity : null;
    }

    public void setMultibuyQuantity(Double multibuyQuantity) {
        this.multibuyQuantity = multibuyQuantity;
    }

    public Map<String, String> getDiscountBundles() {
        return discountBundles;
    }

    public void setDiscountBundles(Map<String, String> discountBundles) {
        this.discountBundles = discountBundles;
    }

    public void addDiscountBundle(String productId, String bundleId) {
        if(this.discountBundles == null) {
            this.discountBundles = new HashMap<>();
        }
        this.lastModifiedDate = new Date();
        this.discountBundles.put(productId, bundleId);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", version=" + version +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", multibuyPrice=" + multibuyPrice +
                ", multibuyQuantity=" + multibuyQuantity +
                ", discountBundles=" + discountBundles +
                '}';
    }
}
