package com.pct.product.domain.basket;

public enum BasketStatus {
    ACTIVE, ARCHIVED
}
