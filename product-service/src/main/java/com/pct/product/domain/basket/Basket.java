package com.pct.product.domain.basket;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pct.product.domain.product.Product;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "baskets")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Basket {

    @Id
    private String id;

    @Version
    private Integer version;

    private Date createdDate;

    private Date lastModifiedDate;

    private String accountId;

    private BasketStatus basketStatus;

    private List<Product> products;

    public Basket() {
    }

    public Basket(BasketStatus basketStatus, Product product, Double quantity, String accountId) {
        this.createdDate = new Date();
        this.basketStatus = basketStatus;
        if (this.products == null) {
            this.products = new ArrayList<>();
        }
        product.setQuantity(quantity);
        this.products.add(product);
        this.accountId = accountId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BasketStatus getBasketStatus() {
        return basketStatus;
    }

    public void setBasketStatus(BasketStatus basketStatus) {
        this.basketStatus = basketStatus;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void addProducts(Product product, Double quantity) {
        if (this.products == null) {
            this.products = new ArrayList<>();
        }
        product.setQuantity(quantity);
        this.products.add(product);
        this.lastModifiedDate = new Date();
    }

    @Override
    public String toString() {
        return "Basket{" +
                "id='" + id + '\'' +
                ", version=" + version +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                ", accountId='" + accountId + '\'' +
                ", basketStatus=" + basketStatus +
                ", products=" + products +
                '}';
    }
}
