package com.pct.product.domain.basket;

public enum BasketAction {
    ADD, REMOVE
}
