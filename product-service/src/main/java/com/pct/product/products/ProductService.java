package com.pct.product.products;

import com.pct.product.domain.product.Product;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ProductRepository productRepository;

    public Product add(Product product) {
        log.info("User {} is adding {} to the system.", getCurrentUserNameFromSecurityContext(), product.getName());
        Product newProduct = new Product(
                product.getName(),
                product.getPrice(),
                product.getMultibuyPrice(),
                product.getMultibuyQuantity());
        return productRepository.save(newProduct);
    }

    public List<Product> getAll() {
        return productRepository.findAll();
    }

    private String getCurrentUserNameFromSecurityContext() {
        return StringUtils.isNotEmpty(SecurityContextHolder.getContext().getAuthentication().getName()) ?
                SecurityContextHolder.getContext().getAuthentication().getName() :
                "unknown context";
    }
}