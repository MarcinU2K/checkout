package com.pct.product.products;

import com.pct.product.domain.product.Product;
import com.pct.product.domain.product.ProductErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class ProductController {

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ProductService productService;

    @PostMapping("/add")
    public ResponseEntity<Product> add(@RequestBody Product product) {
        try {
            return ResponseEntity.ok(productService.add(product));
        } catch (Throwable unhandledException) {
            log.error(unhandledException.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ProductErrorResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    unhandledException.getClass().getSimpleName().toUpperCase(),
                    unhandledException.getMessage()));
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<Product>> getAll() {
        try {
            return ResponseEntity.ok(productService.getAll());
        } catch (Throwable unhandledException) {
            log.error(unhandledException.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Arrays.asList(new ProductErrorResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    unhandledException.getClass().getSimpleName().toUpperCase(),
                    unhandledException.getMessage())));
        }
    }
    //TODO editing/partially editing and removing product endpoints
}