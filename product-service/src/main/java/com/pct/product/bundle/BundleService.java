package com.pct.product.bundle;

import com.pct.product.domain.bundle.Bundle;
import com.pct.product.domain.bundle.BundleErrorResponse;
import com.pct.product.domain.product.Product;
import com.pct.product.exceptions.BundleException;
import com.pct.product.products.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BundleService {

    private static final String PRODUCT_NOT_FOUND = "PRODUCT_NOT_FOUND";
    private static final String THE_PRODUCT_WITH_ID = "The product with id ";
    private static final String HAS_NOT_BEEN_FOUND = " has not been found";

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private BundleRepository bundleRepository;

    @Autowired
    private ProductRepository productRepository;

    public Bundle add(Bundle bundle) throws BundleException {
        List<String> bundledProductIds = bundle.getProducts();
        Bundle newBundle = new Bundle(
                bundle.getSaving(),
                bundledProductIds);
        Bundle savedBundle = bundleRepository.save(newBundle);
        for (String existingProductId : bundledProductIds) {
            log.info("existingProductId: " + existingProductId);
            Product existingProduct = productRepository.findById(existingProductId).orElseThrow(() -> new BundleException(new BundleErrorResponse(
                    404,
                    PRODUCT_NOT_FOUND,
                    THE_PRODUCT_WITH_ID + existingProductId + HAS_NOT_BEEN_FOUND)));
            for (String paredProductId : bundledProductIds) {
                log.info("pared: " + paredProductId);
                if(!existingProduct.getId().equals(paredProductId)) {
                    existingProduct.addDiscountBundle(savedBundle.getId(), paredProductId);
                    existingProduct.setLastModifiedDate(new Date());
                }
            }
            productRepository.save(existingProduct);
        }
        return savedBundle;
    }
}