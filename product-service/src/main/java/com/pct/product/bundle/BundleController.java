package com.pct.product.bundle;

import com.pct.product.domain.bundle.Bundle;
import com.pct.product.domain.bundle.BundleErrorResponse;
import com.pct.product.exceptions.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/bundle")
@RestController
public class BundleController {

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private BundleService bundleService;

    @PostMapping("/add")
    public ResponseEntity<Bundle> add(@RequestBody Bundle bundle) {
        try {
            return ResponseEntity.ok(bundleService.add(bundle));
        } catch (BundleException bundleException) {
            return ResponseEntity.status(bundleException.getErrorResponse().getCode()).body(bundleException.getErrorResponse());
        } catch (Throwable unhandledException) {
            log.error(unhandledException.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new BundleErrorResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    unhandledException.getClass().getSimpleName().toUpperCase(),
                    unhandledException.getMessage()));
        }
    }

    //TODO editing/partially editing and removing bundle endpoints

}
