package com.pct.product.bundle;

import com.pct.product.domain.bundle.Bundle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BundleRepository extends MongoRepository<Bundle, String> {
}
