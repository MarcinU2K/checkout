package com.pct.product.exceptions;

import com.pct.product.domain.basket.BasketErrorResponse;

public class BasketException extends Exception{

    private final BasketErrorResponse errorResponse;

    public BasketException(BasketErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public BasketException(String message, Throwable cause, BasketErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public BasketException(String message, BasketErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public BasketException(Throwable cause, BasketErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public BasketErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}