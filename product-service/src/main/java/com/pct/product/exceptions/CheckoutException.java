package com.pct.product.exceptions;

import com.pct.product.domain.checkout.CheckoutErrorResponse;

public class CheckoutException extends Exception{

    private final CheckoutErrorResponse errorResponse;

    public CheckoutException(CheckoutErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public CheckoutException(String message, Throwable cause, CheckoutErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public CheckoutException(String message, CheckoutErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public CheckoutException(Throwable cause, CheckoutErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public CheckoutErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}