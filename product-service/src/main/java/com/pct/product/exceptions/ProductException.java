package com.pct.product.exceptions;

import com.pct.product.domain.product.ProductErrorResponse;

public class ProductException extends Exception{

    private final ProductErrorResponse errorResponse;

    public ProductException(ProductErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public ProductException(String message, Throwable cause, ProductErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public ProductException(String message, ProductErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public ProductException(Throwable cause, ProductErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public ProductErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}