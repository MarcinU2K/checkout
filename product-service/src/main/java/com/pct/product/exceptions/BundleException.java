package com.pct.product.exceptions;

import com.pct.product.domain.bundle.BundleErrorResponse;

public class BundleException extends Exception{

    private final BundleErrorResponse errorResponse;

    public BundleException(BundleErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public BundleException(String message, Throwable cause, BundleErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public BundleException(String message, BundleErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public BundleException(Throwable cause, BundleErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public BundleErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}