package com.pct.auth.service.security;

import com.pct.auth.domain.User;
import com.pct.auth.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MongoUserDetailsService implements UserDetailsService {

	Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		log.info("Geting data for user {}", username);
		User user = userRepository.findById(username).orElseThrow(()->new UsernameNotFoundException(username));
		user.setAuthorities(getAuthorities(user.getRoles()));
		return user;
	}

	private Set<GrantedAuthority> getAuthorities(Set<String> roles) {
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

		if(roles != null && !roles.isEmpty()) {
			for (String role : roles) {
				log.info("Role {}", role);
				grantedAuthorities.add(new SimpleGrantedAuthority(role));
			}
		}
		return grantedAuthorities;
	}
}
