#!/usr/bin/env bash
sudo docker login
sleep 3
mvn clean install -Dmaven.test.skip=true

sudo docker build -t marcinu2l/checkout:config config/
sudo docker build -t marcinu2l/checkout:registry registry/
sudo docker build -t marcinu2l/checkout:gateway gateway/
sudo docker build -t marcinu2l/checkout:auth-service auth-service/
sudo docker build -t marcinu2l/checkout:account-service account-service/
sudo docker build -t marcinu2l/checkout:product-service product-service/

sudo docker build -t marcinu2l/checkout:mongodb mongodb/

sudo docker push marcinu2l/checkout:config
sudo docker push marcinu2l/checkout:registry
sudo docker push marcinu2l/checkout:gateway
sudo docker push marcinu2l/checkout:auth-service
sudo docker push marcinu2l/checkout:account-service
sudo docker push marcinu2l/checkout:product-service

sudo docker push marcinu2l/checkout:mongodb
