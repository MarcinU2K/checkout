package com.pct.account.repository;

import com.pct.account.domain.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataMongoTest
public class AccountRepositoryTest {

	@Autowired
	private AccountRepository repository;

	@Test
	public void shouldFindAccountByName() {

		Account account = getAccount();
		repository.save(account);

		Account found = repository.findByName(account.getName());
		assertEquals(account.getLastSeen(), found.getLastSeen());
	}

	private Account getAccount() {
		Account account = new Account();
		account.setName("test");
		account.setLastSeen(new Date());

		return account;
	}
}