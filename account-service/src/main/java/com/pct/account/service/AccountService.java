package com.pct.account.service;

import com.pct.account.client.AuthServiceClient;
import com.pct.account.domain.Account;
import com.pct.account.domain.User;
import com.pct.account.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Date;

@Service
public class AccountService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private AuthServiceClient authServiceClient;

	@Autowired
	private AccountRepository accountRepository;

	public Account findByName(String accountName) {
		return accountRepository.findByName(accountName);
	}

	public Account create(User user) {

		Account existing = accountRepository.findByName(user.getUsername());
		Assert.isNull(existing, "account already exists: " + user.getUsername());

		authServiceClient.createUser(user);

		Account account = new Account();
		account.setName(user.getUsername());
		account.setLastSeen(new Date());

		accountRepository.save(account);

		log.info("new account has been created: " + account.getName());

		return account;
	}
}
