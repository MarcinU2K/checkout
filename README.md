# Checkout Management System

Microservice architecture application containing Gateway API, Discovery, Access Token, Health Check API, Externalized Configuration and Database per service patterns. The application has been designed to help control, manage and maintain checkout systems.

## Getting Started

Before running the scripts install:

```

- maven
- docker
- docker-compose

```

Simply use system_start.sh file to run the application and system_stop.sh to stop it. 

The application comes with preloaded test data such as user, products and bundle. All of the endpoint are fully secured by oauth2 and therefore an access token will be required.

**Test credentials:
Login: admin
Password: password

## Endpoints

- **(POST) /uaa/oauth/token** - getting access token

        Header: Authorization: Basic YnJvd3Nlcjo=, Content-Type: application/x-www-form-urlencoded

        Body: scope: ui, username: admin, password: password, grant_type: password

- **(POST) /accounts/** - creating new account

        Header: Content-Type: application/json

        Body: {"username": yourUsername, "password": yourPassword}

- **(POST) /products/add** - adding new product

        Header: Authorization: Bearer GENERATED_TOKEN_FROM_TOP_ENDPOINT

        Body: {"name": yourProductName, "price": yourProductPrice}

- **(GET) /products/all** - getting all products

        Header: Authorization: Bearer GENERATED_TOKEN_FROM_TOP_ENDPOINT

- **(POST) /products/basket/add/{productId}/quantity/{quantity}** - adding product to basket

        Header: Authorization: Bearer GENERATED_TOKEN_FROM_TOP_ENDPOINT

- **(POST) /products/bundle/add** - adding new product bundle

        Header: Authorization: Bearer GENERATED_TOKEN_FROM_TOP_ENDPOINT

        Body: {"saving": yourSavingAmount, "products": [yourProductId1, yourProductId2]}

- **(PUT) /products/checkout/{userId}** - checkout calculations

        Header: Authorization: Bearer GENERATED_TOKEN_FROM_TOP_ENDPOINT

## Built With

* [Spring](https://spring.io/) - The framework used

* [Maven](https://maven.apache.org/) - Dependency Management

* [MongoDb](https://www.mongodb.com/) - Database

## Author

* **Marcin Pisera** - [marcinu2k@gmail.com](mailto:marcinu2k@gmail.com)

## License

This project is licensed under the MIT License
