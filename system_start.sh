#!/usr/bin/env bash
./build.sh

export CONFIG_SERVICE_PASSWORD='CONFIG_SERVICE_PASSWORD'
export ACCOUNT_SERVICE_PASSWORD='ACCOUNT_SERVICE_PASSWORD'
export PRODUCT_SERVICE_PASSWORD='PRODUCT_SERVICE_PASSWORD'
export MONGODB_PASSWORD='MONGODB_PASSWORD'

docker-compose -f docker-compose.yml -f docker-compose.local.yml up > logz.log &