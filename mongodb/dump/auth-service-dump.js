print('dump start');

db.users.update(
    { "_id": "admin" },
    {
        "_id" : "admin",
        "password" : "$2a$10$nXFSe.8iqYVaG6L82Q9dcuAPeG7R0yCtNpTaU1VQAD7pDG7Pe9pwi"
    },
    { upsert: true }
);

print('dump complete');