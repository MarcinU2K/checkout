print('dump start');

db.products.update(
    {"name": "Banana"},
    {
        "id": "5c2d4c8325ff9976643d82cd",
        "version": 0,
        "createdDate": new Date(),
        "_class": "com.pct.product.domain.product.Product",
        "name": "Banana",
        "price": 110,
        "multibuyPrice": 220,
        "multibuyQuantity": 3
    },
    {upsert: true}
);

db.products.update(
    {"name": "Orange Juice"},
    {
        "id": "5c2d4c8325ff9976643d82cf",
        "version": 0,
        "createdDate": new Date(),
        "_class": "com.pct.product.domain.product.Product",
        "name": "Orange Juice",
        "price": 210,
        "multibuyPrice": 380,
        "multibuyQuantity": 2,
        "discountBundles": {
            "5c2d5725d02e7d00010bdd45": "5c2d55c99c35a2857785ac1b"
        }
    },
    {upsert: true}
);

db.products.update(
    {"name": "Crisps"},
    {
        "id": "5c2d4c8325ff9976643d82d1",
        "version": 0,
        "createdDate": new Date(),
        "_class": "com.pct.product.domain.product.Product",
        "name": "Crisps",
        "price": 70,
        "multibuyPrice": 600,
        "multibuyQuantity": 10,
        "discountBundles": {
            "5c2d5725d02e7d00010bdd45": "5c2d4c8325ff9976643d82cf"
        }
    },
    {upsert: true}
);

db.products.update(
    {"name": "Ice cream"},
    {
        "id": "5c2d55c99c35a2857785ac1d",
        "version": 0,
        "createdDate": new Date(),
        "_class": "com.pct.product.domain.product.Product",
        "name": "Ice cream",
        "price": 150
    },
    {upsert: true}
);

db.bundles.update(
    {"id": "5c2d5725d02e7d00010bdd45"},
    {
        "id": "5c2d5725d02e7d00010bdd45",
        "version": 0,
        "saving": 85,
        "_class": "com.pct.product.domain.bundle.Bundle",
        "products": [
            "5c2d4c8325ff9976643d82d1",
            "5c2d4c8325ff9976643d82cf"
        ]
    },
    {upsert: true}
);

print('dump complete');